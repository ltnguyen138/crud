package com.example.demo14.controller;

import com.example.demo14.dto.response.ResFileUpload;
import com.example.demo14.utility.FileDownloadUtil;
import com.example.demo14.utility.FileUploadUtil;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;

@RestController
@RequestMapping("/api/v1")
public class FileHandlingController {

    @PostMapping("/upload")
    public ResponseEntity<?> uploadFile(@RequestParam(name = "file")MultipartFile multipartFile) throws IOException {
        String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
        long size = multipartFile.getSize();
        String type = multipartFile.getContentType();
        FileUploadUtil.saveFile(fileName,multipartFile);
        String url = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/api/v1/download/")
                .path(fileName)
                .toUriString();

        ResFileUpload resFileUpload = new ResFileUpload(fileName,url,size);
        return new ResponseEntity<>(resFileUpload, HttpStatus.OK);
    }
    @GetMapping("/downloadFile/{fileName}")
    public ResponseEntity<?> downloadFile(@PathVariable("fileName") String fileName) throws IOException {


        Resource resource = null;
        resource = FileDownloadUtil.getFileResource(fileName);

        String contentType = "application/octet-stream";
        String headerValue = "attachment; filename=\"" + resource.getFilename() + "\"";

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, headerValue)
                .body(resource);
    }


}
