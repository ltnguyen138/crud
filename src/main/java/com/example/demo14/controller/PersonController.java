package com.example.demo14.controller;

import com.example.demo14.dto.mapping.PersonMapper;
import com.example.demo14.dto.request.ReqPerson;
import com.example.demo14.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/person")
public class PersonController {
    @Autowired
    PersonService personService;
    @Autowired
    PersonMapper personMapper;

    @PostMapping()
    public  ResponseEntity<?> savePerson(@Valid @RequestBody  ReqPerson reqPerson){
        return new ResponseEntity<>(personService.savePerson(reqPerson),HttpStatus.CREATED);
    }

    @GetMapping()
    public  ResponseEntity<?> getPersonPage(Pageable pageable){
        return new ResponseEntity<>(personService.getPersonPage(pageable),HttpStatus.OK);
    }

    @GetMapping("/search")
    public ResponseEntity<?> searchPersonPage(Pageable pageable,@RequestParam(name = "key") String keyword) {
        return  new ResponseEntity<>(personService.searchPerson(keyword,pageable),HttpStatus.OK);
    }

    @GetMapping("/filter")
    public ResponseEntity<?> filterPersonPage(Pageable pageable,@RequestParam(name = "city") String city) {
        return  new ResponseEntity<>(personService.filterPersonByAddress(city,pageable),HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getPersonById(@PathVariable("id") Integer id){
        return new ResponseEntity<>(personService.getPersonById(id),HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public  ResponseEntity<?> updatePerson(@Valid @RequestBody ReqPerson reqPerson, @PathVariable("id") Integer id){
        return  new ResponseEntity<>(personService.updatePerson(reqPerson,id), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public  ResponseEntity<?> deletePerson(@PathVariable("id") Integer id){
        personService.deletePerson(id);
        return  new ResponseEntity<>("deleted successfully",HttpStatus.OK);
    }
}
