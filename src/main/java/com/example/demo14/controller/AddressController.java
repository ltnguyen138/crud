package com.example.demo14.controller;

import com.example.demo14.dto.request.ReqAddress;
import com.example.demo14.dto.response.ResAddress;
import com.example.demo14.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/address")
public class AddressController {
    @Autowired
    AddressService addressService;

    @PostMapping()
    public ResponseEntity<?> saveAddress(@RequestBody @Valid ReqAddress reqAddress){
        return new ResponseEntity<>(addressService.saveAddress(reqAddress), HttpStatus.CREATED);
    }

    @GetMapping()
    public ResponseEntity<?> getAddressPage(Pageable pageable) {

        return  new ResponseEntity<>(addressService.getAllPage(pageable),HttpStatus.OK);
    }

    @GetMapping("/search")
    public ResponseEntity<?> searchAddressPage(Pageable pageable,@RequestParam(name = "key") String keyword) {

        return  new ResponseEntity<>(addressService.searchAddress(pageable,keyword),HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getAddressById(@PathVariable(name = "id") Integer id){
        return  new ResponseEntity<>(addressService.getAddressById(id),HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public  ResponseEntity<?> updateAddress(@PathVariable(name = "id") Integer id, @RequestBody @Valid ReqAddress reqAddress){

        return  new ResponseEntity<>(addressService.updateAddress(id,reqAddress),HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public  ResponseEntity<?> deleteAddress(@PathVariable(name = "id") Integer id){
        addressService.deleteAddressById(id);
        return  new ResponseEntity<>("deleted successfully",HttpStatus.OK);
    }


}
