package com.example.demo14.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter@Setter
@AllArgsConstructor
public class ExceptionMessage {
    private int statusCode;
    private Date timesTamp;
    private String message;
    private String description;
}
