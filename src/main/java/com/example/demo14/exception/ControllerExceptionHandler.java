package com.example.demo14.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;

@ControllerAdvice
public class ControllerExceptionHandler {

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ExceptionMessage> NotFoundExceptionHandler(ResourceNotFoundException e,WebRequest webRequest){
        ExceptionMessage exceptionMessage = new ExceptionMessage(
                HttpStatus.NOT_FOUND.value()
                , new Date()
                , e.getMessage()
                , webRequest.getDescription(false));
        return  new ResponseEntity<>(exceptionMessage, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<ExceptionMessage> adRequestExceptionHandler(BadRequestException e,WebRequest webRequest){
        ExceptionMessage exceptionMessage = new ExceptionMessage(
                HttpStatus.BAD_REQUEST.value()
                , new Date()
                , e.getMessage()
                , webRequest.getDescription(false));
        return  new ResponseEntity<>(exceptionMessage, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ExceptionMessage> globalExceptionHandler(Exception ex, WebRequest webRequest){
        ExceptionMessage exceptionMessage = new ExceptionMessage(
                HttpStatus.BAD_REQUEST.value()
                , new Date()
                , ex.getMessage()
                , webRequest.getDescription(false));
        return new ResponseEntity<>(exceptionMessage,HttpStatus.BAD_REQUEST);
    }
}
