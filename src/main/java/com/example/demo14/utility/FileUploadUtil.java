package com.example.demo14.utility;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;


public class FileUploadUtil {
    public static final Path dirPath = Paths.get("file_upload");

    public static String saveFile(String fileName, MultipartFile multipartFile) throws IOException{

        if(!Files.exists(dirPath)){
            Files.createDirectories(dirPath);
        }
        try(InputStream inputStream = multipartFile.getInputStream()) {

            Path filePath = dirPath.resolve(fileName);
            Files.copy(inputStream,filePath, StandardCopyOption.REPLACE_EXISTING);

        }catch (IOException e){
            throw new IOException("Can not save file");
        }
        return fileName;
    }
}
