package com.example.demo14.utility;

import com.example.demo14.exception.ResourceNotFoundException;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class FileDownloadUtil {
    public static final Path dirPath = Paths.get("file_upload");

    private static Path findFile(String fileName) throws IOException {

        return Files.list(dirPath)
                .filter(file -> file.getFileName().toString().equals(fileName))
                .findFirst()
                .orElse(null);
    }

    public  static Resource getFileResource(String fileName) throws IOException{
        Path foundFile = findFile(fileName);
        if (foundFile==null) throw new ResourceNotFoundException("not found file name: "+fileName);
        return new UrlResource(foundFile.toUri());
    }
}
