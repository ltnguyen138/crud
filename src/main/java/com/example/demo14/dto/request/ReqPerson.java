package com.example.demo14.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ReqPerson {

    @NotBlank
    private String name;
    @Min(value = 0, message = "Age must be greater than or equal to 0")
    private int age;
    @Min(value = 0)
    private  Integer addressId;
}
