package com.example.demo14.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ResAddress {
    @JsonProperty("id_address")
    private  Integer id;
    private  String city;

}
