package com.example.demo14.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ResPerson {
    @JsonProperty("id_person")
    private  Integer id;
    @JsonProperty("name_person")
    private String name;
    @JsonProperty("age_person")
    private int age;
    @JsonProperty("address_person")
    private  String address;
}
