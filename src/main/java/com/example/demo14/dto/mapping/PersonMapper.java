package com.example.demo14.dto.mapping;

import com.example.demo14.dto.request.ReqPerson;
import com.example.demo14.dto.response.ResPerson;
import com.example.demo14.entity.Person;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface PersonMapper {
    Person reqPersonToPerson(ReqPerson reqPerson);
    ReqPerson personToReqPerson(Person person);
    @Mapping(source = "address.city", target = "address")
    ResPerson personToResPerson(Person person);
    @Mapping(source = "address", target = "address.city")
    Person resPersonToPerson(ResPerson resPerson);
}
