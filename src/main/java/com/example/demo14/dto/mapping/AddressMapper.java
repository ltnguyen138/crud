package com.example.demo14.dto.mapping;

import com.example.demo14.dto.request.ReqAddress;
import com.example.demo14.dto.response.ResAddress;
import com.example.demo14.entity.Address;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AddressMapper {
    Address reqAddressToAddress(ReqAddress reqAddress );
    ReqAddress addressToReqAddress(Address address);
    ResAddress addressToResAddress(Address address);
    Address resAddressToAddress(ResAddress resAddress);

}
