package com.example.demo14.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.RepresentationModel;
import org.yaml.snakeyaml.representer.Representer;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Greeting  extends RepresentationModel<Greeting> {

    private String contents;
}
