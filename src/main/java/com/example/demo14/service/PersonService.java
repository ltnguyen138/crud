package com.example.demo14.service;

import com.example.demo14.dto.request.ReqPerson;
import com.example.demo14.dto.response.ResAddress;
import com.example.demo14.dto.response.ResPerson;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public interface PersonService {
    ResPerson savePerson(ReqPerson reqPerson);
    Page<ResPerson> getPersonPage(Pageable pageable);

    ResPerson getPersonById(Integer id);
    ResPerson updatePerson(ReqPerson reqPerson, Integer id);
    void deletePerson(Integer id);
    public Page<ResPerson> searchPerson(String keyword, Pageable pageable);
    public  Page<ResPerson> filterPersonByAddress(String address, Pageable pageable);
}
