package com.example.demo14.service;

import com.example.demo14.dto.mapping.AddressMapper;
import com.example.demo14.dto.request.ReqAddress;
import com.example.demo14.dto.response.ResAddress;
import com.example.demo14.entity.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public interface AddressService {
    public ResAddress saveAddress(ReqAddress reqAddress);
    public Page<ResAddress> getAllPage(Pageable pageable);
    public ResAddress updateAddress(Integer id, ReqAddress reqAddress);
    public  ResAddress getAddressById(Integer id);
    public Page<ResAddress> searchAddress(Pageable pageable, String keyword);
    public  void deleteAddressById(Integer id);

}
