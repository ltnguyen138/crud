package com.example.demo14.service.impl;

import com.example.demo14.dto.mapping.PersonMapper;
import com.example.demo14.dto.request.ReqPerson;
import com.example.demo14.dto.response.ResPerson;
import com.example.demo14.entity.Address;
import com.example.demo14.entity.Person;
import com.example.demo14.exception.ResourceNotFoundException;
import com.example.demo14.repository.AddressRepository;
import com.example.demo14.repository.PersonRepository;
import com.example.demo14.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

@Component
public class PersonServiceImpl implements PersonService {
    @Autowired
    PersonRepository personRepository;
    @Autowired
    AddressRepository addressRepository;
    @Autowired
    PersonMapper personMapper;

    @Override
    public ResPerson savePerson(ReqPerson reqPerson) {
        Person person = personMapper.reqPersonToPerson(reqPerson);

        Integer addressId = reqPerson.getAddressId();
        Address address = addressRepository.findById(addressId)
                .orElseThrow(()->new ResourceNotFoundException("Create person is unsuccessful,not found address with id : " + addressId));

        person.setAddress(address);
        Person savePerson = personRepository.save(person);
        return personMapper.personToResPerson(savePerson);
    }

    @Override
    public Page<ResPerson> getPersonPage(Pageable pageable) {

        return personRepository.findAll(pageable).map(personMapper::personToResPerson);
    }

    @Override
    public ResPerson getPersonById(Integer id) {
        Person person = personRepository.findById(id)
                .orElseThrow(()-> new ResourceNotFoundException("not found person with id : "+ id));
        return  personMapper.personToResPerson(person);
    }

    @Override
    public ResPerson updatePerson(ReqPerson reqPerson, Integer id) {
        Person person = personRepository.findById(id)
                .orElseThrow(()-> new ResourceNotFoundException("not found person with id : "+ id));

        person.setName(reqPerson.getName());
        person.setAge(reqPerson.getAge());
        if(!person.getAddress().getId().equals(reqPerson.getAddressId())  ){
            person.setAddress(addressRepository.findById(reqPerson.getAddressId())
                    .orElseThrow(()->new ResourceNotFoundException("not found address with id : " + reqPerson.getAddressId())
            ));
        }
        personRepository.save(person);
        return personMapper.personToResPerson(person);
    }

    @Override
    public void deletePerson(Integer id) {
        Person person = personRepository.findById(id)
                .orElseThrow(()-> new ResourceNotFoundException("not found person with id : "+ id));
        personRepository.delete(person);
    }

    @Override
    public Page<ResPerson> searchPerson(String keyword, Pageable pageable) {
        return personRepository.searchPerson(keyword,pageable).map(personMapper::personToResPerson);
    }

    @Override
    public Page<ResPerson> filterPersonByAddress(String address, Pageable pageable) {
        return personRepository.findByAddress_City(address,pageable).map(personMapper::personToResPerson);
    }
}
