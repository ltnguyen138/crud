package com.example.demo14.service.impl;

import com.example.demo14.dto.mapping.AddressMapper;
import com.example.demo14.dto.request.ReqAddress;
import com.example.demo14.dto.response.ResAddress;
import com.example.demo14.entity.Address;
import com.example.demo14.exception.BadRequestException;
import com.example.demo14.exception.ResourceNotFoundException;
import com.example.demo14.repository.AddressRepository;
import com.example.demo14.repository.PersonRepository;
import com.example.demo14.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class AddressServiceImpl implements AddressService {
    @Autowired
    AddressMapper addressMapper;
    @Autowired
    AddressRepository addressRepository;
    @Autowired
    PersonRepository personRepository;

    @Override
    public ResAddress saveAddress(ReqAddress reqAddress) {
        Address address = addressRepository.save(addressMapper.reqAddressToAddress(reqAddress));
        return addressMapper.addressToResAddress(address);
    }

    @Override
    public Page<ResAddress> getAllPage(Pageable pageable) {
        return addressRepository
                .findAllPage(pageable)
                .map(addressMapper::addressToResAddress);
    }

    @Override
    public ResAddress updateAddress(Integer id, ReqAddress reqAddress) {
        Address address = addressRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Address not exist with id : " + id));
        address.setCity(reqAddress.getCity());
        addressRepository.save(address);
        return addressMapper.addressToResAddress(address);
    }

    @Override
    public ResAddress getAddressById(Integer id) {
        Address address = addressRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Address not exist with id : " + id));
        return addressMapper.addressToResAddress(address);
    }

    @Override
    public Page<ResAddress> searchAddress(Pageable pageable, String keyword) {
        return addressRepository
                .searchAddress(pageable,keyword)
                .map(addressMapper::addressToResAddress);
    }

    @Override
    public void deleteAddressById(Integer id) {
        Address address = addressRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Address not exist with id : " + id));
        if(personRepository.existsByAddress_Id(id)){
            throw new BadRequestException("Not delete Address with id: "+id);
        }
        addressRepository.delete(address);
    }

}
