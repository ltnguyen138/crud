package com.example.demo14.repository;

import com.example.demo14.entity.Address;
import com.example.demo14.entity.Person;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface PersonRepository extends JpaRepository<Person, Integer> {

    @Query("SELECT  p FROM Person p")
    Page<Person> findAllPage(Pageable pageable);
    @Query("SELECT p FROM Person p where p.name like %:keyword% or p.address.city like %:keyword%")
    Page<Person> searchPerson(@Param("keyword") String keyword, Pageable pageable);
    Page<Person> findByAddress_City(String address_city, Pageable pageable);
    boolean existsByAddress_Id(Integer address_id);

}
