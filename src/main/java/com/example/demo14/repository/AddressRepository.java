package com.example.demo14.repository;

import com.example.demo14.entity.Address;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface AddressRepository extends CrudRepository<Address, Long> {
    @Query("SELECT a FROM Address a")
    Page<Address> findAllPage(Pageable pageable);
    Optional<Address> findById(Integer id);

    @Query("SELECT a FROM  Address a WHERE a.city LIKE %:keyword% ")
    Page<Address> searchAddress(Pageable pageable,@Param("keyword") String keyword);
}
